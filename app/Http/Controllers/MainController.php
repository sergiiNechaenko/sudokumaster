<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function  index(Request $request){
        $input = $request->all();


        $sudokuArray = array(
            array(null,null,null,null,null,null,null,null,null),
            array(null,null,null,null,null,null,null,null,null),
            array(null,null,null,null,null,null,null,null,null),
            array(null,null,null,null,null,null,null,null,null),
            array(null,null,null,null,null,null,null,null,null),
            array(null,null,null,null,null,null,null,null,null),
            array(null,null,null,null,null,null,null,null,null),
            array(null,null,null,null,null,null,null,null,null),
            array(null,null,null,null,null,null,null,null,null),
        );

        $i = $j = 0;
        foreach ($input as $item){
            $sudokuArray[$i][$j] = $item;
            if($j == 8){
                $j = 0;
                $i ++;
            }else{
                $j++;
            }
        }



        for ($i = 0; $i < 9; $i++){
            for ($j = 0; $j < 9; $j++) {
                if(is_null($sudokuArray[$i][$j])){
                    $possibleX = $possibleY = $possibleCube = array(1,2,3,4,5,6,7,8,9);

                    for ($pj = 0; $pj < 9;$pj++){
                        if(in_array($sudokuArray[$i][$pj],$possibleX))
                            unset($possibleX[$sudokuArray[$i][$pj]-1]);
                    }

                    for ($pi = 0; $pi < 9;$pi++){
                        if(in_array($sudokuArray[$pi][$j],$possibleY))
                            unset($possibleY[$sudokuArray[$pi][$j]-1]);
                    }

                    if($i<3){
                        $MaxI = 3;
                        $MinI = 0;
                    }else {
                        if ($i < 6) {
                            $MaxI = 6;
                            $MinI = 3;
                        } else {
                            $MaxI = 9;
                            $MinI = 6;
                        }
                    }

                    if($j<3){
                        $MaxJ = 3;
                        $MinJ = 0;
                    }else {
                        if ($j < 6) {
                            $MaxJ = 6;
                            $MinJ = 3;
                        } else {
                            $MaxJ = 9;
                            $MinJ = 6;
                        }
                    }

                    for ($pi = $MinI; $pi < $MaxI;$pi++){
                        for ($pj = $MinJ; $pj < $MaxJ;$pj++) {
                            if(in_array($sudokuArray[$pi][$pj],$possibleCube))
                                unset($possibleCube[$sudokuArray[$pi][$pj]-1]);
                        }
                    }

                    $possible = array_intersect($possibleX,$possibleY,$possibleCube);
                    if(count($possible) == 1){
                        $sudokuArray[$i][$j] = array_shift($possible);
                    }
                }
            }
        }

        return view('main', compact('sudokuArray'));
    }
}
